package com.slalla.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<String> s1, s2;
    List<Integer> images = Arrays.asList(R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24, R.drawable.ic_baseline_accessible_forward_24,
            R.drawable.ic_baseline_accessible_forward_24);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        s1 = new ArrayList<String>();
        s2 = new ArrayList<String>();
        List<String> temps1 = Arrays.asList(getResources().getStringArray(R.array.programming_languages));
        for(String s : temps1){
            s1.add(s);
        }

        List<String> temps2 = Arrays.asList(getResources().getStringArray(R.array.programming_descriptions));
        for(String s : temps2){
            s2.add(s);
        }
        recyclerView = findViewById(R.id.recycle);

        MyAdapter myAdapter = new MyAdapter((Activity)this, this, (ArrayList)s1, (ArrayList)s2, images);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}