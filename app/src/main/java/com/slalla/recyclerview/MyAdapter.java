package com.slalla.recyclerview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    Context ct;
    ArrayList<String> s1;
    ArrayList<String> s2;
    List<Integer> images;
    boolean isEnable = false;
    boolean isSelectAll = false;
    MainViewModel mainViewModel;
    ArrayList<String> selectList;
    Activity activity;

    public MyAdapter(Activity activity, Context ct, ArrayList<String> s1, ArrayList<String> s2, List<Integer>images) {
        this.activity = activity;
        this.ct = ct;
        this.s1 = s1;
        this.s2 = s2;
        this.images = images;
        selectList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ct);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        mainViewModel = ViewModelProviders.of((FragmentActivity)activity).get(MainViewModel.class);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.myText1.setText(s1.get(position));
        holder.myText2.setText(s2.get(position));
        holder.myImage.setImageResource(images.get(position));


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(!isEnable){
                    ActionMode.Callback callback = new ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                            MenuInflater menuInflater = mode.getMenuInflater();

                            menuInflater.inflate(R.menu.menu, menu);
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                            isEnable = true;

                            ClickItem(holder);

                            mainViewModel.getText().observe((LifecycleOwner)activity, new Observer<String>() {
                                @Override
                                public void onChanged(String s) {
                                    mode. setTitle(String.format("%s Selected", s));
                                }
                            });
                            return true;
                        }

                        @Override
                        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                            int id = item.getItemId();
                            switch (id){
                                case R.id.menu_delete:
                                    for (String s : selectList){
                                        int index = s1.indexOf(s);
                                        Log.d("This is a test", s +" " +index);
                                        s1.remove(index);
                                        s2.remove(index);
                                    }
                                    mode.finish();
                                    break;
                                case R.id.menu_select_all:
                                    if(selectList.size() == s1.size()){
                                        isSelectAll = false;
                                        selectList.clear();
                                    }
                                    else{
                                        isSelectAll = true;
                                        selectList.clear();
                                        for (int i = 0; i <s1.size(); i++){
                                            selectList.add(s1.get(i));
                                        }
                                    }
                                    mainViewModel.setText(selectList.size() +"");
                                    notifyDataSetChanged();
                                    break;
                            }
                            return true;
                        }

                        @Override
                        public void onDestroyActionMode(ActionMode mode) {
                            isEnable = false;
                            isSelectAll = false;
                            selectList.clear();
                            notifyDataSetChanged();
                        }
                    };
                    ((AppCompatActivity) v.getContext()).startActionMode(callback);
                }
                else{
                    ClickItem(holder);
                }
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEnable){
                    ClickItem(holder);
                }
                else{
                    Intent intent = new Intent(ct, MainActivity2.class);
                    intent.putExtra("Language", s1.get(position));
                    ct.startActivity(intent);                }
            }
        });

        if(isSelectAll){
            holder.checkbox.setVisibility(View.VISIBLE);
            holder.itemView.setBackgroundColor(Color.LTGRAY);
        }
        else{
            holder.checkbox.setVisibility(View.GONE);
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void ClickItem(MyViewHolder holder) {
        String s = s1.get(holder.getAdapterPosition());

        if (holder.checkbox.getVisibility() == View.GONE) {
            holder.checkbox.setVisibility(View.VISIBLE);
            holder.itemView.setBackgroundColor(Color.LTGRAY);
            selectList.add(s);
        }
        else{
            holder.checkbox.setVisibility(View.GONE);
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            selectList.remove(s);
        }
        mainViewModel.setText(""+selectList.size());
    }

    @Override
    public int getItemCount() {
        return s1.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView myImage;
        TextView myText1, myText2;
        ConstraintLayout mainLayout;
        ImageView checkbox;

        public MyViewHolder(@NonNull View itemView){
            super(itemView);
            myText1 = itemView.findViewById(R.id.titleAtt);
            myText2 = itemView.findViewById(R.id.desc);
            myImage = itemView.findViewById(R.id.image);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            checkbox = itemView.findViewById(R.id.imageView);
        }
    }
}
